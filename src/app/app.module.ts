import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from "@angular/elements";
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NetworxFormComponent } from './networx-form/networx-form.component';
import { Globals } from './common/Globals';

@NgModule({
  declarations: [
    AppComponent,
    NetworxFormComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    HttpClientModule,
    FormsModule
  ],
  providers: [Globals],

  entryComponents:[AppComponent, NetworxFormComponent]
})
export class AppModule {

  constructor(private injector: Injector){
    const el = createCustomElement(NetworxFormComponent, { injector  });
    customElements.define('networx-form', el);
  }

  ngDoBootstrap() {
  }

}
