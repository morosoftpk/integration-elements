import { TestBed, inject } from '@angular/core/testing';

import { NetworxService } from './networx.service';

describe('NetworxService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NetworxService]
    });
  });

  it('should be created', inject([NetworxService], (service: NetworxService) => {
    expect(service).toBeTruthy();
  }));
});
