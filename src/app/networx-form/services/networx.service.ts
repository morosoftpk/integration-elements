import { Injectable } from '@angular/core';
import { Globals } from '../../common/Globals';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { NgxXml2jsonService } from 'ngx-xml2json';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' }),
  responseType: 'text' as 'text'
};

const httpOptions2 = {
  headers: new HttpHeaders({ 'Access-Control-Allow-Origin': '*',
   'Content-Type': 'application/x-www-form-urlencoded','Accept': 'application/json' }),
   responseType:'text' as 'text',

  // Content-Type': 'application/json',
  //     'Accept': 'application/json',
  //     'Access-Control-Allow-Origin': '*'


  // responseType: 'text' as 'text'
};

// httpOptions.append('heello', 'tttt');

@Injectable({
  providedIn: 'root'
})

export class NetworxService {

  constructor(private global: Globals,
              private http: HttpClient) { }

  postLead(data:any):Observable<any>{
    debugger;
    let urlEncoded="";
    for(let val in data){
      urlEncoded +=val+'='+data[val]+'&';
    }
    return this.http.post(this.global.networxAPI, urlEncoded, httpOptions );
  }

  getRedTrackPostback(clickId: string):Observable<any>{
    debugger;
    return this.http.get('/postback?clickid='+clickId);
    // return this.http.request("GET")
    // this.http.get
  }

  getRedTrackPostbackImage(clickId: string):Observable<any>{
    debugger;
    return this.http.get('//rdtrck2.com/postback?format=img');
    // return this.http.request("GET")
    // this.http.get
  }}
