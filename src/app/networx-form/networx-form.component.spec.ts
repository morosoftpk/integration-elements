import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworxFormComponent } from './networx-form.component';

describe('NetworxFormComponent', () => {
  let component: NetworxFormComponent;
  let fixture: ComponentFixture<NetworxFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NetworxFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworxFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
