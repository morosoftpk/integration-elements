import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { NetworxService } from './services/networx.service';
import { NgxXml2jsonService } from 'ngx-xml2json';
import { Globals } from '../common/Globals';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'networx-form',
  templateUrl: './networx-form.component.html',
  styleUrls: ['./networx-form.component.css']
})

export class NetworxFormComponent implements OnInit,  AfterViewInit{
  @Input() servicetype: string;
  @Input() title: string;
  @Input('customdomain') customDomain: string;

  formStep: number = 1;
  model: any = {};
  loading: boolean = false;
  error: string  = '';
  success: string ='';

  networxId: string;
  networxAccessKey: string;

  selectedService: any = [];
  formTitle: string = 'Get Free Quotes from Approved Local Contractors';
  rtClickID: string ='';


  leadGenerated: boolean = false;
  currentTime: any = new Date().getMilliseconds();

  imageUrl="//rdtrck2.com/postback?format=img&ver="+this.currentTime;

  constructor(
    private _networx: NetworxService,
    private ngxXml2jsonService: NgxXml2jsonService,
    private globals: Globals,
    ) {
  }

  ngOnInit() {
    debugger;

    var urlParams = new URLSearchParams(window.location.search);

    if(urlParams.has('rtClickID')){
      this.rtClickID = urlParams.get('rtClickID');
      console.log('redtrack clieck Id', this.rtClickID);
    } else{
      console.log('No Track Id found.');
    }

    this.networxId = this.globals.networxId;
    this.networxAccessKey = this.globals.networxAccessKey;
    this.selectedService = this.globals.mainServices;

  }

  changeFormStep(step: number) {
    this.formStep = step;
  }

  formatPhoneNo(elem: HTMLInputElement) {
    let phone: string;
    if(!elem.value)
      return;
    
    let val = elem.value.replace('(','');
    val = val.replace(')','');
    val = val.replace('-','');
    val = val.replace(' ','');

    let temp = +val; 
    
    let areaCode: string = val.substring(0,3);
    let exchang: string = val.substring(3,6);
    let tail: string = val.substring(6,10);

    if(val.length > 2 && !isNaN(temp)) {
      if(areaCode.length == 3)
        phone = '('+ areaCode + ') ';
      if(exchang.length > 0 )
        phone += exchang;
      if(exchang.length == 3 )
        phone += '-';
      if(tail.length > 0)
        phone += tail;
    }
    else {
      phone = elem.value;
    }
    this.model.phone = phone;
  }


  ngAfterViewInit(){
    debugger;
    console.log('service type',this.servicetype);
    if(this.title !='' && this.title != undefined && this.title != null)
      this.formTitle = this.title;

   // this.selectedService = this.serviceType;

    switch (this.servicetype) {

      case 'appliances':
      this.selectedService = this.globals.appliances;
      break;

      case 'airDucts':
      this.selectedService = this.globals.airDucts;
      break;

      case 'gutters':
      this.selectedService = this.globals.gutters;
      break;

      case 'powerWashing':
      this.selectedService = this.globals.powerWashing;
      break;

      case 'siding':
      this.selectedService = this.globals.siding;
      break;

      case 'tileGroutCleaning':
      this.selectedService = this.globals.tileGroutCleaning;
      break;

      case 'windows':
      this.selectedService = this.globals.windows;
      break;

      case 'carpetInstall':
        this.selectedService = this.globals.carpetInstall;
      break;
      case 'garageFloor':
        this.selectedService = this.globals.garageFloor;
      break;

      case 'countertops':
        this.selectedService = this.globals.countertops;
      break;

      case 'homeBuilders':
        this.selectedService = this.globals.homeBuilders;
      break;

      case 'paving':
        this.selectedService = this.globals.paving;
      break;

      case 'waterProofing':
        this.selectedService = this.globals.waterProofing;
      break;

      case 'mainServices':
        this.selectedService = this.globals.mainServices;
        break;
      
      case 'windowsInstallReplace':
        this.selectedService = this.globals.windowsInstallReplace;
      break;

      case 'cabinets':
        this.selectedService = this.globals.cabinets;
      break;

      case 'doors':
        this.selectedService = this.globals.doors;
      break;

      case 'decksPorches':
        this.selectedService = this.globals.decksPorches;
      break;

      case 'generalContactors':
        this.selectedService = this.globals.generalContactors;
      break;

      case 'carpenters':
        this.selectedService = this.globals.carpenters;
      break;

      case 'carpetCleaning':
        this.selectedService = this.globals.carpetCleaning;
      break;

      case 'cleaning':
        this.selectedService = this.globals.cleaning;
        break;

      case 'ceilingFan':
        this.selectedService = this.globals.ceilingFan;
      break;
        

      case 'concrete':
        this.selectedService = this.globals.concrete;
        break;

      case 'electricians':
        this.selectedService = this.globals.electricians;
        break;

      case 'exterminators':
        this.selectedService = this.globals.exterminators;
        break;

      case 'fence':
        this.selectedService = this.globals.fence;
        break;

      case 'floors':
        this.selectedService = this.globals.floors;
        break;

      case 'garageDoors':
        this.selectedService = this.globals.garageDoors;
      break;

      case 'drywallPlastering':
        this.selectedService = this.globals.drywallPlastering;
      break;

      case 'heatingFurnace':
        this.selectedService = this.globals.heatingFurnace;
      break;

      case 'handyman':
        this.selectedService = this.globals.handyman;
        break;

      case 'hvac':
        this.selectedService = this.globals.hvac;
        break;

      case 'landScaping':
        this.selectedService = this.globals.landScaping;
        break;

      case 'painting':
        this.selectedService = this.globals.painting;
        break;

      case 'plumbing':
        this.selectedService = this.globals.plumbing;
      break;

      case 'bathroomRemodeling':
        this.selectedService = this.globals.bathroomRemodeling;
      break;

      case 'kitchenRemodeling':
        this.selectedService = this.globals.kitchenRemodeling;
      break;

      case 'remodelingMajorRenovations':
        this.selectedService = this.globals.remodelingMajorRenovations;
      break;

      case 'restoration':
        this.selectedService = this.globals.restoration;
      break;

      case 'roofing':
        this.selectedService = this.globals.roofing;
      break;

      case 'sheds':
        this.selectedService = this.globals.sheds;
      break;

      case 'solar':
        this.selectedService = this.globals.solar;
      break;

      case 'tile':
        this.selectedService = this.globals.tile;
      break;

      case 'treeServices':
        this.selectedService = this.globals.treeServices;
      break;

      default:
        this.selectedService = this.globals.mainServices;
        break;
    }

    if(this.customDomain !='' &&  this.customDomain != undefined && this.customDomain != null){
      debugger;
      this.imageUrl = '//'+this.customDomain+'/postback?format=img&ver='+this.currentTime;
    }

    console.log('customdomain is',this.imageUrl);
  }

  onSubmit(form: NgForm) {
    console.log('Form values in networx component', form);
    this.loading = true;
    this.error = '';
    this.success = '';

    this._networx.postLead(form.value).subscribe(
      res =>{
        debugger;
        this.loading = false
        const parser1 = new DOMParser();
        const xml1 = parser1.parseFromString(res, 'text/xml');
        const jsonObj = this.ngxXml2jsonService.xmlToJson(xml1);

        console.log('post data response',res);
        console.log('post Json data response',jsonObj);
        let successCode: string = jsonObj['affiliateResponse'].successCode;
        this.success = 'Lead has been generated with code '+successCode

        // RedTrak Postback

        if(this.rtClickID !='' &&  this.rtClickID != undefined && this.rtClickID != null  ){

          this.leadGenerated = true;
          // let url = this.globals.rtDefaultURL+'rtClickID='+this.rtClickID+'&sub1=nxCode: '+successCode;

          // this._networx.getRedTrackPostback(url).subscribe(
          //   res=>{
          //     console.log('response is', res)
          //     console.log('Lead has been tracked as well');
          //   },
          //   err=>{
          //     console.log('Some error occurred', err);
          //   }
          // )
        } else{
          console.log('No Click Id found');
        };


      },

      err=>{
        debugger;
        this.loading = false;
        const parserError = new DOMParser();
        const xmlError = parserError.parseFromString(err.error, 'text/xml');
        const jsonObj = this.ngxXml2jsonService.xmlToJson(xmlError);
        console.error('errror is', err);
        console.log('post error is in json',jsonObj);
        this.error = jsonObj['affiliateResponse'].errorMessage
      }
    );
  }
}
