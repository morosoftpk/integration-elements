import { Injectable } from '@angular/core';

@Injectable()
export class Globals{
  networxAPI= 'https://api.networx.com';
  networxId: string = '10599';
  networxAccessKey: string = '652550db1b';
  rtDefaultURL: string = 'http://rdtrck2.com/postback?';  


// Sub Categories of Main Services as per reqeusted


  appliances	=   [
    {'name':'Appliances - Install', 'code': '248'},
    {'name':'Appliances - Repair/Service','code':'247'},
  ];

  airDucts	=   [
    {'name':'Ducts/Vents - Clean', 'code': '20'},
  ];

  gutters	=   [
    {'name': 'Copper Gutters - Install ' , 'code':'397'},
    {'name': 'Copper Gutters - Repair ' , 'code':'407'},
    {'name': 'Galvanized Gutters - Install ' , 'code':'399'},
    {'name': 'Galvanized Gutters - Repair ' , 'code':'409'},
    {'name': 'Gutter Cleaning ' , 'code':'295'},
    {'name': 'PVC Gutters - Install ' , 'code':'401'},
    {'name': 'PVC Gutters - Repair ' , 'code':'411'},
    {'name': 'Wood Gutters - Install ' , 'code':'405'},
    {'name': 'Wood Gutters - Repair ' , 'code':'415'},
    {'name': 'Seamless Metal Gutters - Install ' , 'code':'403'},
    {'name': 'Seamless Metal Gutters - Repair ' , 'code':'413'},
  ];

  powerWashing	=   [
    {'name':'Powerwashing Exterior', 'code': '61'},
  ];


  siding	=   [
    {'name':'Siding Install/Replace', 'code': '13'},
    {'name':'Siding Repair	 ','code':'239'},
  ];


  tileGroutCleaning=   [
    {'name':'Tile & Grout Cleaning', 'code': '118'},
  ];

  windows	=   [
    {'name':'Window Install - Single', 'code': '256'},
    {'name':'Window Repair - Frame & Glass','code':'266'},
    {'name':'Windows Install - Multiple','code':'257'},
  ];

  carpetInstall=[
    {'name':'Carpet - Repair/Refasten 	 ','code':'250'},
    {'name':'Carpet Install 	 ','code':'249'},
  ]

  concrete=[
    {'name':'Concrete Foundation - Install 	 ','code':'391'},
    {'name':'Concrete Foundation - Repair 	 ','code':'417'},
    {'name':'Concrete - Applications 	' ,'code':'63'},
    {'name':'Concrete Driveways/Floors - Install 	' ,'code':'74'},
    {'name':'Concrete Flatwork - Repair & Resurface 	' ,'code':'68'},
    {'name':'Concrete Patios, Walks & Steps - Install', 'code': '351'},
    {'name':'Concrete Resurfacing 	' ,'code':'69'},
    {'name':'Concrete Retaining Walls 	' ,'code':'70'},
    {'name':'Overlays/Stamped 	' ,'code':'66'},
    {'name':'Polishing Concrete 	' ,'code':'82'},
    {'name':'Poured Concrete Wall - Install 	 ','code':'353'},
  ]

  garageFloor = [
    {'name':'Driveways/Floors - Repair 	' ,'code':'75'},
    {'name':'Garage Floor Coatings 	' ,'code':'77'},
  ]


  countertops = [
    {'name':'Countertop Repair Solid Surface (Concrete Stainless Steel..) 	 ','code':'427'},
    {'name':'Countertops Install (Granite, Marble, Quartz) 	 ','code':'423'},
    {'name':'Steel Countertops - Install 	 ','code':'431'},
    {'name':'Stone Slab Countertops - Install 	 ','code':'421'},
    {'name':'Stone Slab Countertops - Repair (Granite, Marble, Quartz...) 	 ','code':'425'},
    {'name':'Laminate Countertops - Install 	 ','code':'419'},
    {'name':'Laminate Countertops - Repair 	 ','code':'429'},
  ]

  homeBuilders = [
    {'name':'New Home Construction 	 ','code':'145'},
  ]

  paving = [
    {'name':'Pavers - Patios, Walks & Steps - Install 	 ','code':'357'},
    {'name':'Interlocking Pavers-Driveways, Floors Install 	 ','code':'355'},
    {'name':'Brick & Stone Flatwork - Repair   	 ','code':'395'}, 
  ]

  waterProofing = [
    {'name':'Foundation Or Basement - Waterproofing 	 ','code':'393'},
  ];

  mainServices =[
    {'name':'Copper Gutters - Install 	 ','code':'397'},
    {'name':'Copper Gutters - Repair 	 ','code':'407'}, 
    {'name':'Ducts/Vents - Clean 	','code':' 20'},
    {'name':'Galvanized Gutters - Install 	 ','code':'399'},
    {'name':'Galvanized Gutters - Repair 	 ','code':'409'},
    {'name':'Gutter Cleaning 	 ','code':'295'},
    {'name':'Powerwashing Exterior 	','code':' 61'},
    {'name':'PVC Gutters - Install 	 ','code':'401'},
    {'name':'PVC Gutters - Repair 	 ','code':'411'},
    {'name':'Seamless Metal Gutters - Install 	 ','code':'403'},
    {'name':'Seamless Metal Gutters - Repair 	 ','code':'413'},
    {'name':'Siding Install/Replace 	','code':' 13'},
    {'name':'Siding Repair 	 ','code':'239'},
    {'name':'Soil, Sand, Mulch And Rock Delivery 	 ','code':'465'},
    {'name':'Tile & Grout Cleaning 	 ','code':'118'},
    {'name':'Wood Gutters - Install 	','code':' 405'},
    {'name':'Wood Gutters - Repair','code':' 415'},
  ];

  windowsInstallReplace = [
    {'name':'Window Install - Single 	 ','code':'256'},
    {'name':'Window Repair - Frame & Glass 	 ','code':'266'},
    {'name':'Windows Install - Multiple 	 ','code':'257'},
    {'name':'Window Frame-Repair 	 ','code':'265'},
    {'name':'Window Repair 	 ','code':'255'},
  ]


  solar = [
    {'name':'Solar Panel Install 	 ','code':'193'},
  ]

  cabinets=[
    {'name':'Cabinets/Drawers Installation 	','code' :'38'},
    {'name':'Cabinets/Drawers Repair 	','code' :'39'},
    {'name':'Custom Cabinets 	 ','code':'252'},
  ]

  doors = [
    {'name':'Door Install 	','code' :'44'},
    {'name':'Door Repair', 'code': '45'},
  ]

  decksPorches = [
    {'name':'Decks/Porches/Ramps - Build 	','code' :'42'},
    {'name':'Decks/Porches/Ramps - Repair 	','code' :'43'},
  ];

  generalContactors = [
    {'name':'General Contractors 	 ','code':'154'},
  ];

  carpenters=[
    {'name':'Closets/Built-in Furniture 	','code' :'40'},
    {'name':'Custom Carpentry/Woodworking 	','code' :'41'},
    {'name':'Finish/Trim/Molding 	','code' :'46'},
    {'name':'Frame New Construction/Additions 	','code' :'48'},
    {'name':'Framing 	','code' :'47'},
    {'name':'Pergola/Arbor/Trellis - Build 	','code' :'50'},
    {'name':'Wood Stairs/Railings - Install/Replace 	','code' :'49'},
  ]

  carpetCleaning=[
    {'name':'Carpet Cleaning 	', 'code': '251'},
  ]


  cleaning = [
    {'name':'Maid Services 	 ','code':'54'},
    {'name':'Move In/Out 	 ','code':'55'},
    {'name':'Office/Industrial 	 ','code':'58'},
    {'name':'Organizing/Declutter 	 ','code':'56'},
    {'name':'Post Construction 	 ','code':'57'},
  ];

  ceilingFan= [
    {'name':'Ceiling Fan - Install 	 ','code':'186'},
    {'name':'Ceiling Fan - Repair 	 ','code':'187'},
  ]

  electricians= [
    {'name':'Attic/Whole House Fan Install 	 ','code':'184'},
    {'name':'Attic/Whole House Fan Repair 	 ','code':'185'},
    {'name':'Electrical For Home Addition/Remodel 	 ','code':'179'},
    {'name':'Electrical Panel Upgrade 	 ','code':'195'},
    {'name':'Electrical Wiring/Rewiring 	 ','code':'178'},
    {'name':'Exhaust Fan - Install 	 ','code':'190'},
    {'name':'Generator - Install 	 ','code':'180'},
    {'name':'Hot Tubs/Jacuzzi/Spa - Install 	 ','code':'191'},
    {'name':'Interior Lighting - Install 	 ','code':'188'},
    {'name':'Smoke Detector - Install 	 ','code':'192'},
    {'name':'Switches/Outlets/Fixtures - Install 	 ','code':'177'},
    {'name':'Switches/Outlets/Fixtures - Repair 	 ','code':'194'},
    {'name':'Troubleshooting 	 ','code':'183'},
    {'name':'Generator Service/Repair 	 ','code':'181'},
  ];

  exterminators = [
    {'name':'Ants 	 ','code':'97'},
    {'name':'Bed Bugs/Fleas/Ticks 	 ','code':'98'},
    {'name':'Birds And Bats 	 ','code':'99'},
    {'name':'Flies / Mosquitoes 	 ','code':'100'},
    {'name':'Fumigation 	 ','code':'101'},
    {'name':'Insects 	 ','code':'103'},
    {'name':'Mice/Rodents 	 ','code':'104'},
    {'name':'Mites 	 ','code':'272'},
    {'name':'Pest Control 	 ','code':'102'},
    {'name':'Roaches 	 ','code':'105'},
    {'name':'Spiders/Crickets 	 ','code':'106'},
    {'name':'Stinging Insects 	 ','code':'107'},
    {'name':'Stinkbugs 	 ','code':'108'},
    {'name':'Termites 	 ','code':'109'},
    {'name':'Wildlife Removal 	 ','code':'110'},
  ];


  fence =[
    {'name':'Aluminum/Steel Fence Install 	 ','code':'171'},
    {'name':'Aluminum/Steel Fence Repair 	 ','code':'172'},
    {'name':'Barbed Wire Fence Install 	 ','code':'173'},
    {'name':'Barbed Wire Fence Repair 	 ','code':'174'},
    {'name':'Chain Link Fence Install 	 ','code':'168'},
    {'name':'Chain Link Fence Repair/Alter 	 ','code':'169'},
    {'name':'Vinyl/PVC Fence Install 	 ','code':'166'},
    {'name':'Vinyl/PVC Fence Repair 	 ','code':'167'},
    {'name':'Wood Fence Install 	 ','code':'164'},
    {'name':'Wood Fence Repair 	 ','code':'165'},
    {'name':'Wrought Iron Fence Install 	 ','code':'170'},
  ];

  floors =[
    {'name':'Hardwood Floor - Install 	 ','code':'156'},
    {'name':'Laminate Flooring - Install 	 ','code':'159'},
    {'name':'Laminate Flooring - Repair 	 ','code':'160'},
    {'name':'Vinyl/Linoleum Floor - Install 	 ','code':'161'},
    {'name':'Vinyl/Linoleum Floor - Repair 	 ','code':'162'},
    {'name':'Wood Floor - Repair Or Partially Replace 	 ','code':'157'},
    {'name':'Wood Floor Refinishing 	 ','code':'158'},
  ];

  garageDoors = [
    {'name':'Garage-Doors	Broken Hinges 	 ','code':'85'},
    {'name':'Garage-Doors	Garage Door Install 	 ','code':'86'},
    {'name':'Garage-Doors	Garage Door Repair 	 ','code':'87'},
    {'name':'Garage-Doors	Garage Door Service 	 ','code':'90'},
    {'name':'Garage-Doors	Opener/Remote - Install 	 ','code':'88'},
    {'name':'Garage-Doors	Opener/Remote - Repair 	 ','code':'89'},
    {'name':'Garage-Doors	Panel Replacement/Repair 	 ','code':'91'},
    {'name':'Garage-Doors	Rolling Grille Install 	 ','code':'92'},
    {'name':'Garage-Doors	Rolling Grille Repair 	 ','code':'93'},
    {'name':'Garage-Doors	Spring Replacement 	 ','code':'94'},
    {'name':'Garage-Doors	Weather Stripping/Insulation 	 ','code':'95'},
  ];

  drywallPlastering = [
    {'name':'Drywall Installation 	 ','code':'221'},
    {'name':'Drywall Repair/Patching 	 ','code':'236'},
    {'name':'Install Or Replace Stucco 	 ','code':'297'},
    {'name':'Repair - Stucco 	 ','code':'299'},
    {'name':'Plastering 	 ','code':'237'},
    {'name':'Wallpaper Hanging/Removal 	 ','code':'214'},
    {'name':'Popcorn Ceiling Removal 	 ','code':'220'},
  ]


  handyman	=   [ ];

  heatingFurnace = [
    {'name':'Boiler Install/Replace 	 ','code':'24'},
    {'name':'Boiler Repair/Service 	 ','code':'25'},
    {'name':'Electric Furnace - Install 	 ','code':'435'},
    {'name':'Electric Furnace - Repair 	 ','code':'441'},
    {'name':'Gas Furnace - Install 	 ','code':'437'},
    {'name':'Gas Furnace - Repair 	 ','code':'443'},
    {'name':'Heat Pump Install 	 ','code':'29'},
    {'name':'Heat Pump Repair 	 ','code':'30'},
    {'name':'Electrical Baseboard/Wall Heater - Install 	 ','code':'32'},
    {'name':'Electrical Baseboard/Wall Heater - Repair 	 ','code':'33'},
    {'name':'Oil Furnace - Install 	 ','code':'439'},
    {'name':'Oil Furnace - Repair 	 ','code':'445'},
    {'name':'Propane Furnace - Install 	 ','code':'447'},
    {'name':'Propane Furnace - Repair 	 ','code':'449'},
    {'name':'Radiant Floor Heating System - Repair 	','code':' 35'},
    {'name':'Radiant Floor Heating System - Install 	','code':' 36'},
  ]


  hvac =[
    {'name':'Central A/C - Install/Replace 	 ','code':'27'},
    {'name':'Central A/C - Repair/Service 	 ','code':'26'},
    {'name':'Ductless (mini-split) A/C Install 	 ','code':'34'},
    {'name':'Ductless (mini-split) A/C Service Or Repair 	 ','code':'365'},
    {'name':'Ducts/Vents - Install/Replace 	 ','code':'19'},
    {'name':'Thermostat - Install/Replace 	 ','code':'31'},
  ];

  landScaping = [
    {'name':'Gardening - Bushes/Flowers/etc 	 ','code':'209'},
    {'name':'Hardscapes 	 ','code':'203'},
    {'name':'Landscape Design/Install 	 ','code':'197'},
    {'name':'Landscape Lighting 	 ','code':'204'},
    {'name':'Landscape Repair 	 ','code':'198'},
    {'name':'Lawn Care And Maintenance 	 ','code':'196'},
    {'name':'Lawn Sprinkler System-Install 	 ','code':'267'},
    {'name':'Lawn Sprinkler System-Repair 	 ','code':'268'},
    {'name':'Leaf Removal 	 ','code':'199'},
    {'name':'Sod Installation 	 ','code':'200'},
    {'name':'Soil, Sand, Mulch And Rock Delivery 	 ','code':'465'},
    {'name':'Sprinklers/Irrigation System 	 ','code':'201'},
    {'name':'Water Features 	 ','code':'208'},
  ];

  treeServices = [
    {'name':'Tree Removal/Trimming 	 ','code':'202'},
  ]

  painting =[
    {'name':'Cabinet Painting 	 ','code':'343'},
    {'name':'Exterior Painting 	 ','code':'218'},
    {'name':'Exterior Painting - Whole House 	 ','code':'211'},
    {'name':'Interior Painting 3+ Rooms 	 ','code':'210'},
    {'name':'Interior Painting-1 To 2 Rooms 	 ','code':'270'},
    {'name':'Paint Exterior - Trim/Shutters 	 ','code':'212'},
    {'name':'Paint Removal/Stripping 	 ','code':'216'},
    {'name':'Paint/Stain - Deck/Fence/Porch 	 ','code':'213'},
    {'name':'Painting - Faux Finishes 	 ','code':'349'},
  ];


  plumbing =[
    {'name':'Backflow Preventer Install 	 ','code':'463'},
    {'name':'Bathtub/Shower - Install/Replace 	 ','code':'228'},
    {'name':'Boiler Install 	 ','code':'121'},
    {'name':'Boiler Repair 	 ','code':'122'},
    {'name':'Drain Clog/Blockage - Clear 	 ','code':'123'},
    {'name':'Drain Line Break - Camera Locate 	 ','code':'227'},
    {'name':'Faucets/Fixtures/Pipes Repair/Replace 	 ','code':'235'},
    {'name':'Gas Piping 	 ','code':'131'},
    {'name':'Leak Detection/Repair 	 ','code':'129'},
    {'name':'Plumbing For Addition/Remodel 	 ','code':'132'},
    {'name':'Plumbing Inspection 	 ','code':'461'},
    {'name':'Sewer Main - Clear 	 ','code':'234'},
    {'name':'Sewer Main - Replace/Repair 	 ','code':'233'},
    {'name':'Sump Pump - Repair/Replace 	 ','code':'229'},
    {'name':'Tankless Water Heater - Install 	 ','code':'230'},
    {'name':'Tankless Water Heater - Repair 	 ','code':'231'},
    {'name':'Water Heater - Install/Replace 	 ','code':'136'},
    {'name':'Water Heater - Repair/Service 	 ','code':'137'},
    {'name':'Water Line 	 ','code':'138'},
    {'name':'Water Main - Replace/Repair 	 ','code':'232'},
    {'name':'Water Treatment/Purification 	 ','code':'139'},
    {'name':'Install Or Replace A Septic System 	 ','code':'133'},
    {'name':'Pump Out A Septic Tank 	 ','code':'345'},
    {'name':'Repair A Septic System 	 ','code':'347'},
    {'name':'Fire Sprinkler System 	 ','code':'126'},
  ];

  bathroomRemodeling = [
    {'name':'Bathroom Remodel','code':'152'},
  ];

  kitchenRemodeling = [
    {'name':'Kitchen Remodel 	 ','code':'151'},
  ]

  remodelingMajorRenovations = [
    {'name':'Attic Remodel 	 ','code':'146'},
    {'name':'Basement Remodel 	 ','code':'143'},
    {'name':'Garage Build 	 ','code':'269'},
    {'name':'Garage Remodel 	 ','code':'144'},
    {'name':'General Contractors 	 ','code':'154'},
    {'name':'Home Addition 	 ','code':'147'},
    {'name':'Major Renovation - Multiple Rooms 	 ','code':'148'},
    {'name':'Recovery Services 	 ','code':'293'},
    {'name':'Remodel Or Renovate 1+ Rooms 	 ','code':'153'},
    {'name':'Remodel To Accommodate A Disability 	 ','code':'149'},
  ];

  restoration = [
    {'name':'Storm/Water Restoration 	 ','code':'254'},
    {'name':'Fire Restoration 	 ','code':'246'},
  ]

  roofing =[
    {'name':'Roof Install - Asphalt Shingle 	 ','code':'313'},
    {'name':'Roof Install - Flat/Single Ply 	 ','code':'323'},
    {'name':'Roof Install - Metal 	 ','code':'319'},
    {'name':'Roof Install - Natural Slate 	 ','code':'315'},
    {'name':'Roof Install - Tile 	 ','code':'321'},
    {'name':'Roof Install-Wood Shake/Comp. 	 ','code':'317'},
    {'name':'Roof Repair - Asphalt Shingle 	 ','code':'301'},
    {'name':'Roof Repair - Metal 	 ','code':'307'},
    {'name':'Roof Repair - Natural Slate 	 ','code':'303'},
    {'name':'Roof Repair - Tile 	 ','code':'309'},
    {'name':'Roof Repair - Wood Shake/Comp. 	 ','code':'305'},
    {'name':'Roof Repair-Flat / SinglePly 	 ','code':'311'},
    {'name':'Roof Replace - Asphalt Shingle 	 ','code':'325'},
    {'name':'Roof Replace - Flat/SinglePly 	 ','code':'335'},
    {'name':'Roof Replace - Metal 	 ','code':'331'},
    {'name':'Roof Replace - Naturalsolar Slate 	 ','code':'327'},
    {'name':'Roof Replace - Tile 	 ','code':'333'},
    {'name':'Roof Replace - Wood Shake 	 ','code':'329'},
    {'name':'Skylight Install', 'code': '10'},
  ];

  sheds = [
    {'name':'Shed/Barn/Gazebo Build 	','code' :'51'},
    {'name':'Shed/Barn/Gazebo Repair 	','code' :'52'},
  ]

  tile =[
    {'name':'Backsplash Installation 	 ','code':'120'},
    {'name':'Bathroom/Shower Install 	 ','code':'114'},
    {'name':'Bathroom/Shower Repair 	 ','code':'115'},
    {'name':'Grout Repair/Replacement 	 ','code':'117'},
    {'name':'Natural Stone (Granite/Marble/Slate/etc) 	 ','code':'116'},
    {'name':'Tile Floor Install 	 ','code':'111'},
    {'name':'Tile Floor Repair 	 ','code':'112'},
    {'name':'Walls/Backsplash 	 ','code':'113'},
  ];

}
