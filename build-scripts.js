const fs = require('fs-extra');
const concat = require('concat');

(async function build() {

    const files =[
        './dist/runtime.js',
        './dist/polyfills.js',
        './dist/main.js',
         ];

    await fs.ensureDir('elements')

    await concat(files, './../main-app/ng-networx-element.js')
    console.info('Angular Networx Elements created successfully!')

    // For copying file

    fs.copy('./dist/styles.css', './../main-app/styles.css')
    .then(() => console.log('Css Files copied as well.success!'))
    .catch(err => console.error('could not copy the css file', err));

})()
